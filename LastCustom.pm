# vim:set sw=4 ts=4 sts=4 ft=perl expandtab:
package LastCustom;
use Mojo::Base -strict;

sub custom_filter {
    my $content = shift;
    my $id      = shift;

    # Exemple of modification:
    # $content =~ s/Onoyo/Anoyo/g if $id == 2583066;

    return $content;
}

1;
